<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->
<!-- code_chunk_output -->

* [Git Rocks (http://git.rocks/)](#git-rocks-httpgitrocks)
	* [Making a branch and pushing changes to it](#making-a-branch-and-pushing-changes-to-it)
	* [Merge changes from branch into the "master" branch](#merge-changes-from-branch-into-the-master-branch)
	* [Release Process](#release-process)
* [Learn Git Branching (https://learngitbranching.js.org/)](#learn-git-branching-httpslearngitbranchingjsorg)
	* [Chapter 1, Introduction Sequence](#chapter-1-introduction-sequence)
		* [1.1 Introduction to Git Commits (lesson)](#11-introduction-to-git-commits-lesson)
			* [Git Commits](#git-commits)
			* [Introduction to Git Commits (Demo)](#introduction-to-git-commits-demo)
			* [Introduction to Git Commits (practice)](#introduction-to-git-commits-practice)
		* [1.2 Branching in Git (lesson)](#12-branching-in-git-lesson)
			* [Git Branches](#git-branches)
			* [Branching in Git (Demo)](#branching-in-git-demo)
			* [Branching in Git (practice)](#branching-in-git-practice)
		* [1.3 Merging in Git (lesson)](#13-merging-in-git-lesson)
			* [Branches and Merging](#branches-and-merging)
			* [Merging in Git (Demo #1)](#merging-in-git-demo-1)
			* [Merging in Git (Demo #2)](#merging-in-git-demo-2)
			* [Merging in Git (practice)](#merging-in-git-practice)
		* [1.4 Rebase Introduction (lesson)](#14-rebase-introduction-lesson)
			* [Git Rebase](#git-rebase)
			* [Rebase Introduction (Demo)](#rebase-introduction-demo)
			* [Rebase Introduction (practice)](#rebase-introduction-practice)
	* [Chapter 2, Ramping Up](#chapter-2-ramping-up)
		* [2.1 Detach yo' HEAD (lesson)](#21-detach-yo-head-lesson)
			* [Moving around in Git](#moving-around-in-git)
			* [HEAD](#head)
			* [Detach yo' HEAD (Demo #1)](#detach-yo-head-demo-1)
			* [Detaching HEAD](#detaching-head)
			* [Detach yo' HEAD (Demo #2)](#detach-yo-head-demo-2)
			* [Detach yo' HEAD (practice)](#detach-yo-head-practice)
		* [2.2 Relative Refs (^) (lesson)](#22-relative-refs-lesson)
			* [Relative Refs](#relative-refs)
			* [Relative Refs (^) (Demo #1)](#relative-refs-demo-1)
			* [Relative Refs (^) (Demo #2)](#relative-refs-demo-2)
			* [Relative Refs (^) (practice)](#relative-refs-practice)
		* [2.3 Relative Refs #2 (~) (lesson)](#23-relative-refs-2-~-lesson)
			* [The "~" operator](#the-~-operator)
			* [Relative Refs #2 (~) (Demo #1)](#relative-refs-2-~-demo-1)
			* [Branch forcing](#branch-forcing)
			* [Relative Refs #2 (~) (Demo #2)](#relative-refs-2-~-demo-2)
			* [Relative Refs #2 (~) (practice)](#relative-refs-2-~-practice)
		* [2.4 Reversing Changes in Git (lesson)](#24-reversing-changes-in-git-lesson)
			* [Reversing Changes in Git](#reversing-changes-in-git)
			* [Git Reset](#git-reset)
			* [Reversing Changes in Git (Demo #1)](#reversing-changes-in-git-demo-1)
			* [Git Revert](#git-revert)
			* [Reversing Changes in Git (Demo #2)](#reversing-changes-in-git-demo-2)
			* [Reversing Changes in Git (practice)](#reversing-changes-in-git-practice)
	* [Chapter 3, Moving Work Around](#chapter-3-moving-work-around)
		* [3.1 Cherry-pick Intro (lesson)](#31-cherry-pick-intro-lesson)
			* [Moving Work Around](#moving-work-around)
			* [Git Cherry-pick](#git-cherry-pick)
			* [Cherry-pick Intro (Demo)](#cherry-pick-intro-demo)
			* [Cherry-pick Intro (practice)](#cherry-pick-intro-practice)
		* [3.2 Interactive Rebase Intro (lesson)](#32-interactive-rebase-intro-lesson)
			* [Git Interactive Rebase](#git-interactive-rebase)
			* [Interactive Rebase Intro (Demo)](#interactive-rebase-intro-demo)
			* [Interactive Rebase Intro (practice)](#interactive-rebase-intro-practice)
	* [Chapter 4, A Mixed Bag](#chapter-4-a-mixed-bag)
		* [4.1 Grabbing Just 1 Commit (lesson)](#41-grabbing-just-1-commit-lesson)
			* [Locally stacked commits](#locally-stacked-commits)
			* [Grabbing Just 1 Commit (practice)](#grabbing-just-1-commit-practice)
		* [4.2 Juggling Commits (lesson)](#42-juggling-commits-lesson)
			* [Juggling Commits](#juggling-commits)
			* [Juggling Commits (practice)](#juggling-commits-practice)
		* [4.3 Juggling Commits #2 (lesson)](#43-juggling-commits-2-lesson)
			* [Juggling Commits #2](#juggling-commits-2)
			* [Juggling Commits #2 (Demo)](#juggling-commits-2-demo)
			* [Juggling Commits #2 (practice)](#juggling-commits-2-practice)
		* [4.4 Git Tags (lesson)](#44-git-tags-lesson)
			* [Git Tags](#git-tags)
			* [Git Tags (Demo)](#git-tags-demo)
			* [Git Tags (practice)](#git-tags-practice)
		* [4.5 Git Describe (lesson)](#45-git-describe-lesson)
			* [Git Describe](#git-describe)
			* [Git Describe (Demo)](#git-describe-demo)
			* [Git Describe (practice)](#git-describe-practice)
	* [Chapter 5, Advanced Topics](#chapter-5-advanced-topics)
		* [5.1 Rebasing over 9000 ^times^ (lesson)](#51-rebasing-over-9000-times-lesson)
			* [Rebasing Multiple Branches (practice)](#rebasing-multiple-branches-practice)
		* [5.2 Multiple parents (lesson)](#52-multiple-parents-lesson)
			* [Specifying Parents](#specifying-parents)
			* [Multiple parents (Demo #1)](#multiple-parents-demo-1)
			* [Multiple parents (Demo #2)](#multiple-parents-demo-2)
			* [Multiple parents (Demo #3)](#multiple-parents-demo-3)
			* [Multiple parents (Demo #4)](#multiple-parents-demo-4)
			* [Multiple parents (practice)](#multiple-parents-practice)
		* [5.3 Branch Spaghetti (lesson)](#53-branch-spaghetti-lesson)
			* [Branch Spaghetti (practice)](#branch-spaghetti-practice)
	* [Chapter 6, Push & Pull -- Git Remotes](#chapter-6-push-pull-git-remotes)
		* [6.1 Clone Intro (lesson)](#61-clone-intro-lesson)
			* [Git Remotes](#git-remotes)
			* [Our Command to create remotes](#our-command-to-create-remotes)
			* [Clone Intro (Demo)](#clone-intro-demo)
			* [Clone Intro (practice)](#clone-intro-practice)
		* [6.2 Remote Branches (lesson)](#62-remote-branches-lesson)
			* [Git Remote Branches](#git-remote-branches)
			* [What is `o/`?](#what-is-o)
			* [Remote Branches (Demo)](#remote-branches-demo)
			* [Remote Branches (practice)](#remote-branches-practice)
		* [6.3 Git Fetchin' (lesson)](#63-git-fetchin-lesson)
			* [Git Fetch](#git-fetch)
			* [Git Fetchin' (Demo)](#git-fetchin-demo)
			* [What fetch does](#what-fetch-does)
			* [What fetch doesn't do](#what-fetch-doesnt-do)
			* [Git Fetchin' (practice)](#git-fetchin-practice)
		* [6.4 Git Pullin' (lesson)](#64-git-pullin-lesson)
			* [Git Pull](#git-pull)
			* [Git Pullin' (Demo #1)](#git-pullin-demo-1)
			* [Git Pullin' (Demo #2)](#git-pullin-demo-2)
			* [Git Pullin' (practice)](#git-pullin-practice)
		* [6.5 Faking Teamwork (lesson)](#65-faking-teamwork-lesson)
			* [Simulating collaboration](#simulating-collaboration)
			* [Faking Teamwork (Demo #1)](#faking-teamwork-demo-1)
			* [Faking Teamwork (Demo #2)](#faking-teamwork-demo-2)
			* [Faking Teamwork (practice)](#faking-teamwork-practice)
		* [6.6 Git Pushin' (lesson)](#66-git-pushin-lesson)
			* [Git Push](#git-push)
			* [Git Pushin' (Demo)](#git-pushin-demo)
			* [Git Pushin' (practice)](#git-pushin-practice)
		* [6.7 Diverged History (lesson)](#67-diverged-history-lesson)
			* [Diverged Work](#diverged-work)
			* [Diverged History (Demo #1)](#diverged-history-demo-1)
			* [Diverged History (Demo #2)](#diverged-history-demo-2)
			* [Diverged History (Demo #3)](#diverged-history-demo-3)
			* [Diverged History (Demo #4)](#diverged-history-demo-4)
			* [Diverged History (practice)](#diverged-history-practice)
	* [Chapter 7, To Origin And Beyond -- Advanced Git Remotes](#chapter-7-to-origin-and-beyond-advanced-git-remotes)
		* [7.1 Push Master! (lesson)](#71-push-master-lesson)
			* [Merging feature branches](#merging-feature-branches)
			* [Push Master! (Demo)](#push-master-demo)
			* [Push Master! (practice)](#push-master-practice)
		* [7.2 Merging with remotes (lesson)](#72-merging-with-remotes-lesson)
			* [Why not merge?](#why-not-merge)
			* [Merging with remotes (practice)](#merging-with-remotes-practice)
		* [7.3 Remote Tracking (lesson)](#73-remote-tracking-lesson)
			* [Remote-Tracking branches](#remote-tracking-branches)
			* [Remote tracking](#remote-tracking)
			* [Can I specify this myself?](#can-i-specify-this-myself)
			* [Remote Tracking (Demo #1)](#remote-tracking-demo-1)
			* [Remote Tracking (Demo #2)](#remote-tracking-demo-2)
			* [Way #2](#way-2)
			* [Remote Tracking (Demo #3)](#remote-tracking-demo-3)
			* [Remote Tracking (practice)](#remote-tracking-practice)
		* [7.4 Git push arguments (lesson)](#74-git-push-arguments-lesson)
			* [Push arguments](#push-arguments)
			* [Git push arguments (demo #1)](#git-push-arguments-demo-1)
			* [Git push arguments (demo #2)](#git-push-arguments-demo-2)
			* [Git push arguments (practice)](#git-push-arguments-practice)
		* [7.5 Git push arguments -- Expanded! (lesson)](#75-git-push-arguments-expanded-lesson)
			* [`<place>` argument details](#place-argument-details)
			* [Git push arguments -- Expanded! (demo #1)](#git-push-arguments-expanded-demo-1)
			* [Git push arguments -- Expanded! (demo #2)](#git-push-arguments-expanded-demo-2)
			* [Git push arguments -- Expanded! (practice)](#git-push-arguments-expanded-practice)
		* [7.6 Fetch arguments (lesson)](#76-fetch-arguments-lesson)
			* [Git fetch arguments](#git-fetch-arguments)
			* [The `<place>` parameter](#the-place-parameter)
			* [Fetch arguments (demo #1)](#fetch-arguments-demo-1)
			* [Fetch arguments (demo #2)](#fetch-arguments-demo-2)
			* [Fetch arguments (demo #3)](#fetch-arguments-demo-3)
			* [Fetch arguments (demo #4)](#fetch-arguments-demo-4)
			* [Fetch arguments (practice)](#fetch-arguments-practice)
		* [7.7 Source of nothing (lesson)](#77-source-of-nothing-lesson)
			* [Oddities of `<source>`](#oddities-of-source)
			* [Source of nothing (demo #1)](#source-of-nothing-demo-1)
			* [Source of nothing (demo #2)](#source-of-nothing-demo-2)
			* [Source of nothing (practice)](#source-of-nothing-practice)
		* [7.8 Pull arguments (lesson)](#78-pull-arguments-lesson)
			* [Git pull arguments](#git-pull-arguments)
			* [Pull arguments (demo #1)](#pull-arguments-demo-1)
			* [Pull arguments (demo #2)](#pull-arguments-demo-2)
			* [Pull arguments (practice)](#pull-arguments-practice)

<!-- /code_chunk_output -->


# Git Rocks (http://git.rocks/)

## Making a branch and pushing changes to it

* Make a new branch
`git branch feature1`

* Checkout this new branch to switch to it
`git checkout feature1`

* Push this branch to the remote repository
`git push origin feature1`

* Sync the "master" branch to your new branch
`git pull --rebase origin master`

* Create a new file
`touch feature1.md`

* Add this file to be committed
`git add feature1.md`

* Commit this new file
`git commit -m "Adding a new feature"`

* Push your latest changes to your branch
`git push origin feature1`

## Merge changes from branch into the "master" branch

* Checkout "master" to switch to it
`git checkout master`

* Grab the latest changes from "master"
`git pull --rebase origin master`

* Merge your new branch into "master"
`git merge feature1`

* Push your merge to the remote repository
`git push origin master`

## Release Process

* Create a release branch
`git branch release`

* Checkout this release branch to switch to it
`git checkout release`

* Push this branch to the remote repository
`git push origin release`

* Create your first tag as a release candidate
`git tag 1.0.0.0`

* Push this tag to the remote repository
`git push --tags`

>Note: _There is a new commit on "master" that you would like to include in your release._

* Switch back to the "master" branch
`git checkout master`

* Grab all the latest changes in "master"
`git pull --rebase origin master`

* Go back to your release branch
`git checkout release`

* Grab the one change you want to include
`git cherry-pick 5e5f40c`

* Push it to the remote repository
`git push origin release`

* Tag a new version
`git tag 1.0.0.1`

* Push that new version to the remote repository
`git push --tags`

* You are now ready to push to production, let's create your production tag
`git tag 1.0.0`

* Push that production tag to the remote repository
`git push --tags`

* Resume your work on "master
`git checkout master`

* Delete your local release branch
`git branch -D release`

* Delete the release branch on the remote repository
`git push origin --delete release`

> Note: _The release branch has been deleted, but what if you need to do a hotfix on that version._

* Checkout the tag
`git checkout 1.0.0`

* Create a hotfix branch and switch to it
`git checkout -b hotfix`

> Note: _You have a new hotfix branch. From there you can make your changes and tag a new version._

# Learn Git Branching (https://learngitbranching.js.org/)

## Chapter 1, Introduction Sequence

### 1.1 Introduction to Git Commits (lesson)

#### Git Commits
A commit in a git repository records a snapshot of all the files in your directory. It's like a giant copy and paste, but even better!

Git wants to keep commits as lightweight as possible though, so it doesn't just blindly copy the entire directory every time you commit. It can (when possible) compress a commit as a set of changes, or a "delta", from one version of the repository to the next.

Git also maintains a history of which commits were made when. That's why most commits have ancestor commits above them -- we designate this with arrows in our visualization. Maintaining history is great for everyone working on the project!

It's a lot to take in, but for now you can think of commits as snapshots of the project. Commits are very lightweight and switching between them is wicked fast!

#### Introduction to Git Commits (Demo)
Let's see what this looks like in practice. On the right we have a visualization of a (small) git repository. There are two commits right now -- the first initial commit, `C0`, and one commit after that `C1` that might have some meaningful changes.

![git-commits_1](assets/git-commits_1.jpg)

Type `git commit`

![git-commits_2](assets/git-commits_2.jpg)

There we go! Awesome. We just made changes to the repository and saved them as a commit. The commit we just made has a parent, `C1`, which references which commit it was based off of.

#### Introduction to Git Commits (practice)
* make two commits

![git-commits_question](assets/git-commits_question.jpg)

```
git commit -m "1st commit"
git commit -m "2nd commit"
```

![git-commits_answer](assets/git-commits_answer.jpg)

### 1.2 Branching in Git (lesson)

#### Git Branches

Branches in Git are incredibly lightweight as well. They are simply pointers to a specific commit -- nothing more. This is why many Git enthusiasts chant the mantra:
_"branch early, and branch often"_
Because there is no storage / memory overhead with making many branches, it's easier to logically divide up your work than have big beefy branches.

When we start mixing branches and commits, we will see how these two features combine. For now though, just remember that a branch essentially says "I want to include the work of this commit and all parent commits."

#### Branching in Git (Demo)
Let's see what branches look like in practice.

![git-branches_1](assets/git-branches_1.jpg)

Here we will create a new branch named `newImage`

Type: `git branch newImage`

![git-branches_2](assets/git-branches_2.jpg)

There, that's all there is to branching! The branch `newImage` now refers to commit `C1`

Let's try to put some work on this new branch.

![git-branches_3](assets/git-branches_3.jpg)

Type: git commit`

![git-branches_4](assets/git-branches_4.jpg)

Oh no! The `master` branch moved but the `newImage` branch didn't! That's because we weren't "on" the new branch, which is why the asterisk `(*)` was on `master`

Let's tell git we want to checkout the branch with
`git checkout <name>`
This will put us on the new branch before committing our changes

![git-branches_5](assets/git-branches_5.jpg)

Type: `git checkout newImage; git commit`

![git-branches_6](assets/git-branches_6.jpg)

There we go! Our changes were recorded on the new branch

#### Branching in Git (practice)
* Ok! You are all ready to get branching. Once this window closes, make a new branch named `bugFix` and switch to that branch.

By the way, here's a shortcut: if you want to create a new branch AND check it out at the same time, you can simply type `git checkout -b [yourbranchname]`.

![git-branches_question](assets/git-branches_question.jpg)

```
git branch bugFix
git checkout bugFix
```

![git-branches_answer](assets/git-branches_answer.jpg)

### 1.3 Merging in Git (lesson)

#### Branches and Merging
Great! We now know how to commit and branch. Now we need to learn some kind of way of combining the work from two different branches together. This will allow us to branch off, develop a new feature, and then combine it back in.

The first method to combine work that we will examine is `git merge`. Merging in Git creates a special commit that has two unique parents. A commit with two parents essentially means "I want to include all the work from this parent over here and this one over here, and the set of all their parents."

#### Merging in Git (Demo #1)
It's easier with visuals, let's check it out in the next view

![branches-and-merging_1](assets/branches-and-merging_1.jpg)

Here we have two branches; each has one commit that's unique. This means that neither branch includes the entire set of "work" in the repository that we have done. Let's fix that with merge.

We will `merge` the branch `bugFix` into `master`

Type: `git merge bugFix`

![branches-and-merging_2](assets/branches-and-merging_2.jpg)

Woah! See that? First of all, `master` now points to a commit that has two parents. If you follow the arrows up the commit tree from `master`, you will hit every commit along the way to the root. This means that `master` contains all the work in the repository now.

#### Merging in Git (Demo #2)
Also, see how the colors of the commits changed? To help with learning, I have included some color coordination. Each branch has a unique color. Each commit turns a color that is the blended combination of all the branches that contain that commit.

So here we see that the `master` branch color is blended into all the commits, but the `bugFix` color is not. Let's fix that...

![branches-and-merging_3](assets/branches-and-merging_3.jpg)

Let's merge `master` into `bugFix`:

Type: `git checkout bugFix; git merge master`

![branches-and-merging_4](assets/branches-and-merging_4.jpg)

Since `bugFix` was an ancestor of `master`, git didn't have to do any work; it simply just moved `bugFix` to the same commit `master` was attached to.

Now all the commits are the same color, which means each branch contains all the work in the repository! Woohoo!

#### Merging in Git (practice)
* To complete this level, do the following steps:
  * Make a new branch called `bugFix`
  * Checkout the `bugFix` branch with `git checkout bugFix`
  * Commit once
  * Go back to `master` with `git checkout`
  * Commit another time
  * Merge the branch `bugFix` into `master` with `git merge`

![branches-and-merging_question](assets/branches-and-merging_question.jpg)

```
git branch bugFix
git checkout bugFix
git commit -m "created bugFix"
git checkout master
git commit -m "merging bugFix to master"
git merge bugFix
```

![branches-and-merging_answer](assets/branches-and-merging_answer.jpg)

### 1.4 Rebase Introduction (lesson)

#### Git Rebase
The second way of combining work between branches is _rebasing_. Rebasing essentially takes a set of commits, "copies" them, and plops them down somewhere else.

While this sounds confusing, the advantage of rebasing is that it can be used to make a nice linear sequence of commits. The commit log / history of the repository will be a lot cleaner if only rebasing is allowed.

#### Rebase Introduction (Demo)
Let's see it in action...

Here we have two branches yet again; note that the bugFix branch is currently selected (note the asterisk)

![git-rebase_1](assets/git-rebase_1.jpg)

We would like to move our work from bugFix directly onto the work from master. That way it would look like these two features were developed sequentially, when in reality they were developed in parallel.

Let's do that with the `git rebase` command

Type: `git rebase master`

![git-rebase_2](assets/git-rebase_2.jpg)

Awesome! Now the work from our bugFix branch is right on top of master and we have a nice linear sequence of commits.

Note that the commit `C3` still exists somewhere (it has a faded appearance in the tree), and `C3`' is the "copy" that we rebased onto master.

The only problem is that master hasn't been updated either, let's do that now...

Now we are checked out on the `master` branch. Let's go ahead and rebase onto `bugFix`...

![git-rebase_3](assets/git-rebase_3.jpg)

Type: `git rebase bugfix`

![git-rebase_4](assets/git-rebase_4.jpg)

There! Since `master` was an ancestor of `bugFix`, git simply moved the `master` branch reference forward in history.

#### Rebase Introduction (practice)
* To complete this level, do the following
  * Checkout a new branch named `bugFix`
  * Commit once
  * Go back to `master` and commit again
  * Check out `bugFix` again and rebase onto `master`

![git-rebase_question](assets/git-rebase_question.jpg)

```
git checkout -b bugFix
git commit -m "created bugFix"
git checkout master
git commit -m "going to rebase bugFix onto master"
git checkout bugFix
git rebase master
```

![git-rebase_answer](assets/git-rebase_answer.jpg)

## Chapter 2, Ramping Up

### 2.1 Detach yo' HEAD (lesson)

#### Moving around in Git
Before we get to some of the more advanced features of Git, it's important to understand different ways to move through the commit tree that represents your project.

Once you're comfortable moving around, your powers with other git commands will be amplified!

#### HEAD
First we have to talk about "HEAD". HEAD is the symbolic name for the currently checked out commit -- it's essentially what commit you're working on top of.

HEAD always points to the most recent commit which is reflected in the working tree. Most git commands which make changes to the working tree will start by changing HEAD.

Normally HEAD points to a branch name (like bugFix). When you commit, the status of bugFix is altered and this change is visible through HEAD.

#### Detach yo' HEAD (Demo #1)
Let's see this in action. Here we will reveal HEAD before and after a commit.

![moving-around-in-git_1](assets/moving-around-in-git_1.jpg)

Type: `git checkout C1; git checkout master; git commit; git checkout C2`

![moving-around-in-git_2](assets/moving-around-in-git_2.jpg)

See! HEAD was hiding underneath our `master` branch all along.

#### Detaching HEAD
Detaching HEAD just means attaching it to a commit instead of a branch.

#### Detach yo' HEAD (Demo #2)
This is what it looks like beforehand:

HEAD -> master -> C1

![moving-around-in-git_3](assets/moving-around-in-git_3.jpg)

Type: `git checkout C1`

![moving-around-in-git_4](assets/moving-around-in-git_4.jpg)

And now it's

HEAD -> C1

#### Detach yo' HEAD (practice)

* To complete this level, let's detach HEAD from `bugFix` and attach it to the commit instead.
  * Specify this commit by its hash. The hash for each commit is displayed on the circle that represents the commit.

![moving-around-in-git_question](assets/moving-around-in-git_question.jpg)

```
git checkout C4
```

![moving-around-in-git_answer](assets/moving-around-in-git_answer.jpg)

### 2.2 Relative Refs (^) (lesson)

#### Relative Refs
Moving around in Git by specifying commit hashes can get a bit tedious. In the real world you won't have a nice commit tree visualization next to your terminal, so you'll have to use `git log` to see hashes.

Furthermore, hashes are usually a lot longer in the real Git world as well. For instance, the hash of the commit that introduced the previous level is `fed2da64c0efc5293610bdd892f82a58e8cbc5d8`. Doesn't exactly roll off the tongue...

The upside is that Git is smart about hashes. It only requires you to specify enough characters of the hash until it uniquely identifies the commit. So I can type `fed2` instead of the long string above.

Like I said, specifying commits by their hash isn't the most convenient thing ever, which is why Git has relative refs. They are awesome!

With relative refs, you can start somewhere memorable (like the branch `bugFix` or `HEAD`) and work from there.

Relative commits are powerful, but we will introduce two simple ones here:
* Moving upwards one commit at a time with `^`
* Moving upwards a number of times with `~<num>`

Let's look at the Caret (^) operator first. Each time you append that to a ref name, you are telling Git to find the parent of the specified commit.

So saying `master^` is equivalent to "the first parent of `master`".

`master^^` is the grandparent (second-generation ancestor) of `master`

#### Relative Refs (^) (Demo #1)
Let's check out the commit above master here

![relative-refs_1](assets/relative-refs_1.jpg)

Type: `git checkout master^`

![relative-refs_2](assets/relative-refs_2.jpg)

Boom! Done. Way easier than typing the commit hash

#### Relative Refs (^) (Demo #2)
You can also reference `HEAD` as a relative ref. Let's use that a couple of times to move upwards in the commit tree

![relative-refs_3](assets/relative-refs_3.jpg)

Type: `git checkout C3; git checkout HEAD^; git checkout HEAD^; git checkout HEAD^`

![relative-refs_4](assets/relative-refs_4.jpg)

Easy! We can travel backwards in time with `HEAD^`

#### Relative Refs (^) (practice)
* To complete this level, check out the parent commit of `bugFix`. This will detach `HEAD`.
  * You can specify the hash if you want, but try using relative refs instead!

![relative-refs_question](assets/relative-refs_question.jpg)

```
git checkout bugFix^
```

![relative-refs_answer](assets/relative-refs_answer.jpg)

### 2.3 Relative Refs #2 (~) (lesson)

#### The "~" operator
Say you want to move a lot of levels up in the commit tree. It might be tedious to type `^` several times, so Git also has the tilde (~) operator.

The tilde operator (optionally) takes in a trailing number that specifies the number of parents you would like to ascend. Let's see it in action

#### Relative Refs #2 (~) (Demo #1)
Let's specify a number of commits back with `~`.

![The_~_operator-1](assets/The_~_operator-1.jpg)

Type: `git checkout HEAD~4`

![The_~_operator-2](assets/The_~_operator-2.jpg)

Boom! So concise -- relative refs are great.

#### Branch forcing
You're an expert on relative refs now, so let's actually _use_ them for something.

One of the most common ways I use relative refs is to move branches around. You can directly reassign a branch to a commit with the `-f` option. So something like:

`git branch -f master HEAD~3`

moves (by force) the master branch to three parents behind HEAD.

#### Relative Refs #2 (~) (Demo #2)
Let's see that previous command in action.

![The_~_operator-3](assets/The_~_operator-3.jpg)

Type: `git branch -f master HEAD~3`

![The_~_operator-4](assets/The_~_operator-4.jpg)

There we go! Relative refs gave us a concise way to refer to `C1` and branch forcing (`-f`) gave us a way to quickly move a branch to that location.

#### Relative Refs #2 (~) (practice)
* To complete this level, move `HEAD` to `C1`, `master` to `C6`, and `bugFix` to `C0`.

![The_~_operator-question](assets/The_~_operator-question.jpg)

```
git branch -f master C6
git chackout HEAD^
git branch -f bugFix HEAD~1
```

![The_~_operator-answer](assets/The_~_operator-answer.jpg)

### 2.4 Reversing Changes in Git (lesson)

#### Reversing Changes in Git

There are many ways to reverse changes in Git. And just like committing, reversing changes in Git has both a low-level component (staging individual files or chunks) and a high-level component (how the changes are actually reversed). Our application will focus on the latter.

There are two primary ways to undo changes in Git -- one is using `git reset` and the other is using `git revert`. We will look at each of these in the next dialog

#### Git Reset
`git reset` reverts changes by moving a branch reference backwards in time to an older commit. In this sense you can think of it as "rewriting history;" `git reset` will move a branch backwards as if the commit had never been made in the first place.

#### Reversing Changes in Git (Demo #1)
Let's see what that looks like:

![reversing-changes-in-git_1](assets/reversing-changes-in-git_1.jpg)

Type: `git reset HEAD~1`

![reversing-changes-in-git_2](assets/reversing-changes-in-git_2.jpg)

Nice! Git moved the master branch reference back to `C1`; now our local repository is in a state as if `C2` had never happened.

#### Git Revert
While reseting works great for local branches on your own machine, its method of "rewriting history" doesn't work for remote branches that others are using.

In order to reverse changes and _share_ those reversed changes with others, we need to use `git revert`.

#### Reversing Changes in Git (Demo #2)
Let's see it in action

![reversing-changes-in-git_3](assets/reversing-changes-in-git_3.jpg)

Type: `git revert HEAD`

![reversing-changes-in-git_4](assets/reversing-changes-in-git_4.jpg)

Weird, a new commit plopped down below the commit we wanted to reverse. That's because this new commit `C2'` introduces _changes_ -- it just happens to introduce changes that exactly reverses the commit of `C2`.

With reverting, you can push out your changes to share with others.

#### Reversing Changes in Git (practice)
* To complete this level, reverse the most recent commit on both `local` and `pushed`. You will revert two commits total (one per branch).
  * Keep in mind that `pushed` is a remote branch and `local` is a local branch -- that should help you choose your methods.

![reversing-changes-in-git_question](assets/reversing-changes-in-git_question.jpg)

```
git reset local^
git checkout pushed
git revert pushed
```

![reversing-changes-in-git_answer](assets/reversing-changes-in-git_answer.jpg)

## Chapter 3, Moving Work Around

### 3.1 Cherry-pick Intro (lesson)

#### Moving Work Around
So far we've covered the basics of git -- committing, branching, and moving around in the source tree. Just these concepts are enough to leverage 90% of the power of git repositories and cover the main needs of developers.

That remaining 10%, however, can be quite useful during complex workflows (or when you've gotten yourself into a bind). The next concept we're going to cover is "moving work around" -- in other words, it's a way for developers to say "I want this work here and that work there" in precise, eloquent, flexible ways.

This may seem like a lot, but it's a simple concept.

#### Git Cherry-pick
The first command in this series is called `git cherry-pick`. It takes on the following form:

* `git cherry-pick <Commit1> <Commit2> <...>`

It's a very straightforward way of saying that you would like to copy a series of commits below your current location (`HEAD`). I personally love `cherry-pick` because there is very little magic involved and it's easy to understand.

#### Cherry-pick Intro (Demo)
Let's see a demo!

Here's a repository where we have some work in branch `side` that we want to copy to `master`. This could be accomplished through a rebase (which we have already learned), but let's see how cherry-pick performs.

![moving-work-around_1](assets/moving-work-around_1.jpg)

Type: `git cherry-pick C2 C4`

![moving-work-around_2](assets/moving-work-around_2.jpg)

That's it! We wanted commits `C2` and `C4` and git plopped them down right below us. Simple as that!

#### Cherry-pick Intro (practice)
* To complete this level, simply copy `C3`, `C4`, and `C7` from the three branches shown into master.

![moving-work-around_question](assets/moving-work-around_question.jpg)

```
git cherry-pick C3 C4 C7
```

![moving-work-around_answer](assets/moving-work-around_answer.jpg)

### 3.2 Interactive Rebase Intro (lesson)

#### Git Interactive Rebase
Git cherry-pick is great when you know which commits you want (_and_ you know their corresponding hashes) -- it's hard to beat the simplicity it provides.

But what about the situation where you don't know what commits you want? Thankfully git has you covered there as well! We can use interactive rebasing for this -- it's the best way to review a series of commits you're about to rebase.

Let's dive into the details...

All interactive rebase means is using the `rebase` command with the `-i` option.

If you include this option, git will open up a UI to show you which commits are about to be copied below the target of the rebase. It also shows their commit hashes and messages, which is great for getting a bearing on what's what.

For "real" git, the UI window means opening up a file in a text editor like vim. For our purposes, I've built a small dialog window that behaves the same way.

When the interactive rebase dialog opens, you have the ability to do 3 things:

* You can reorder commits simply by changing their order in the UI (in our window this means dragging and dropping with the mouse).
* You can choose to completely omit some commits. This is designated by `pick` -- toggling `pick` off means you want to drop the commit.
* Lastly, you can squash commits. Unfortunately our levels don't support this for a few logistical reasons, so I'll skip over the details of this. Long story short, though -- it allows you to combine commits.

#### Interactive Rebase Intro (Demo)
Great! Let's see an example.

When you hit the button, an interactive rebase window will appear. Reorder some commits around (or feel free to unpick some) and see the result!

![git-interactive-rebase_1](assets/git-interactive-rebase_1.jpg)

Type: `git rebase -i HEAD~4`

![git-interactive-rebase_3](assets/git-interactive-rebase_3.jpg)

Boom! Git copied down commits in the exact same way you specified through the UI

![git-interactive-rebase_2](assets/git-interactive-rebase_2.jpg)

#### Interactive Rebase Intro (practice)
* To finish this level, do an interactive rebase and achieve the follwoing:
  * Drop `C2`,
  * Reorder the comits so that `C4` comes after `C5`

![git-interactive-rebase_question](assets/git-interactive-rebase_question.jpg)

```
git rebase -i HEAD~4
```

![git-interactive-rebase_answer1](assets/git-interactive-rebase_answer1.jpg)

![git-interactive-rebase_answer2](assets/git-interactive-rebase_answer2.jpg)

## Chapter 4, A Mixed Bag

### 4.1 Grabbing Just 1 Commit (lesson)

#### Locally stacked commits
Here's a development situation that often happens: I'm trying to track down a bug but it is quite elusive. In order to aid in my detective work, I put in a few debug commands and a few print statements.

All of these debugging / print statements are in their own commits. Finally I track down the bug, fix it, and rejoice!

Only problem is that I now need to get my `bugFix` back into the `master` branch. If I simply fast-forwarded `master`, then `master` would get all my debug statements which is undesirable. There has to be another way...

We need to tell git to copy only one of the commits over. This is just like the levels earlier on moving work around -- we can use the same commands:

* `git rebase -i`
* `git cherry-pick`

To achieve this goal.

#### Grabbing Just 1 Commit (practice)
* This is a later level so we will leave it up to you to decide which command you want to use, but in order to complete the level, make sure `master` receives the commit that `bugFix` references.

![grabbing-just-1-commit_question](assets/grabbing-just-1-commit_question.jpg)

```
git checkout master
git cherry-pick C4
```

![grabbing-just-1-commit_answer](assets/grabbing-just-1-commit_answer.jpg)

### 4.2 Juggling Commits (lesson)

#### Juggling Commits
Here's another situation that happens quite commonly. You have some changes (`newImage`) and another set of changes (`caption`) that are related, so they are stacked on top of each other in your repository (aka one after another).

The tricky thing is that sometimes you need to make a small modification to an earlier commit. In this case, design wants us to change the dimensions of `newImage` slightly, even though that commit is way back in our history!!

#### Juggling Commits (practice)
We will overcome this difficulty by doing the following:

* We will re-order the commits so the one we want to change is on top with `git rebase -i`
* We will `git commit --amend` to make the slight modification
* Then we will re-order the commits back to how they were previously with `git rebase -i`
* Finally, we will move master to this updated part of the tree to finish the level (via the method of your choosing)
There are many ways to accomplish this overall goal (I see you eye-ing cherry-pick), and we will see more of them later, but for now let's focus on this technique. Lastly, pay attention to the goal state here -- since we move the commits twice, they both get an apostrophe appended. One more apostrophe is added for the commit we amend, which gives us the final form of the tree

That being said, I can compare levels now based on structure and relative apostrophe differences. As long as your tree's master branch has the same structure and relative apostrophe differences, I'll give full credit

![juggling-commits_question](assets/juggling-commits_question.jpg)

```
git rebase -i HEAD~2 # change order to (C3, C2)
```

![juggling-commits_answer1](assets/juggling-commits_answer1.jpg)
![juggling-commits_answer2](assets/juggling-commits_answer2.jpg)

```
git commit --amend
```

![juggling-commits_answer3](assets/juggling-commits_answer3.jpg)

```
git rebase -i HEAD~2 # change order to (C2'', C3')
```

![juggling-commits_answer4](assets/juggling-commits_answer4.jpg)
![juggling-commits_answer5](assets/juggling-commits_answer5.jpg)

```
git rebase caption master
```

![juggling-commits_answer6](assets/juggling-commits_answer6.jpg)

### 4.3 Juggling Commits #2 (lesson)

#### Juggling Commits #2
If you haven't completed Juggling Commits #1 (the previous level), please do so before continuing

As you saw in the last level, we used `rebase -i` to reorder the commits. Once the commit we wanted to change was on top, we could easily `--amend` it and re-order back to our preferred order.

The only issue here is that there is a lot of reordering going on, which can introduce rebase conflicts. Let's look at another method with `git cherry-pick`.

Remember that git cherry-pick will plop down a commit from anywhere in the tree onto HEAD (as long as that commit isn't an ancestor of HEAD).

#### Juggling Commits #2 (Demo)
Here's a small refresher demo:

![juggling-commits-2_1](assets/juggling-commits-2_1.jpg)

Type: `git cherry-pick C2`

![juggling-commits-2_2](assets/juggling-commits-2_2.jpg)

Nice! Let's move on

#### Juggling Commits #2 (practice)
So in this level, let's accomplish the same objective of amending `C2` once but avoid using `git rebase -i`.

Remember, the exact number of apostrophe's (') on the commit are not important, only the relative differences. For example, I will give credit to a tree that matches the goal tree but has one extra apostrophe everywhere

![juggling-commits-2_question](assets/juggling-commits-2_question.jpg)

```
git checkout master
git cherry-pick C2
```

![juggling-commits-2_answer1](assets/juggling-commits-2_answer1.jpg)

```
git commit --amend
git cherry-pick C3
```

![juggling-commits-2_answer2](assets/juggling-commits-2_answer2.jpg)

### 4.4 Git Tags (lesson)

#### Git Tags
As you have learned from previous lessons, branches are easy to move around and often refer to different commits as work is completed on them. Branches are easily mutated, often temporary, and always changing.

If that's the case, you may be wondering if there's a way to permanently mark historical points in your project's history. For things like major releases and big merges, is there any way to mark these commits with something more permanent than a branch?

You bet there is! Git tags support this exact use case -- they (somewhat) permanently mark certain commits as "milestones" that you can then reference like a branch.

More importantly though, they never move as more commits are created. You can't "check out" a tag and then complete work on that tag -- tags exist as anchors in the commit tree that designate certain spots.

#### Git Tags (Demo)
Let's see what tags look like in practice.

Let's try making a tag at `C1` which is our version 1 prototype

![git-tags_1](assets/git-tags_1.jpg)

Type: `git tag v1 C1`

![git-tags_2](assets/git-tags_2.jpg)

There! Quite easy. We named the tag `v1` and referenced the commit `C1` explicitly.
* If you leave the commit off, git will just use whatever `HEAD` is at

#### Git Tags (practice)
For this level create the tags `v0` for `C1` & `v1` for `C2`, then check `v1` out.
* Notice how you go into detached `HEAD` state -- this is because you can't commit directly onto the `v1` tag.

![git-tags_question](assets/git-tags_question.jpg)

```
git tag v0 C1
git tag v1 C2
git checkout v1
```

![git-tags_answer](assets/git-tags_answer.jpg)

### 4.5 Git Describe (lesson)

#### Git Describe
Because tags serve as such great "anchors" in the codebase, git has a command to _describe_ where you are relative to the closest "anchor" (aka tag). And that command is called `git describe`!

Git describe can help you get your bearings after you've moved many commits backwards or forwards in history; this can happen after you've completed a git bisect (a debugging search) or when sitting down at a coworkers computer who just left on vacation.

Git describe takes the form of:

`git describe <ref>`

Where `<ref>` is anything git can resolve into a commit.
* If you don't specify a ref, git just uses where you're checked out right now (`HEAD`).

The output of the command looks like:

`<tag>_<numCommits>_g<hash>`

Where `<tag>` is the closest ancestor tag in history, `<numCommits>` is how many commits away that tag is, and `<hash>` is the hash of the commit being described.

#### Git Describe (Demo)
Let's look at a quick example. For this tree below:

![git-describe_1](assets/git-describe_1.jpg)

Type: `git tag v2 C3`

![git-describe_2](assets/git-describe_2.jpg)

The command `git describe master` would output:
`v1_2_gC2`

Whereas `git describe side` would output:
`v2_1_gC4`

#### Git Describe (practice)
That's pretty much all there is to git describe! Try describing a few of the locations in this level to get a feel for the command.

Once you're ready, just go ahead and commit once to finish the level.

![git-describe_question](assets/git-describe_question.jpg)

```
$ git describe
v1_2_gC6
$ git describe master
v0_2_gC2
$ git describe side
v1_1_gC4
git commit
```

## Chapter 5, Advanced Topics

### 5.1 Rebasing over 9000 ^times^ (lesson)

#### Rebasing Multiple Branches (practice)
Man, we have a lot of branches going on here! Let's rebase all the work from these branches onto master.

Upper management is making this a bit trickier though -- they want the commits to all be in sequential order. So this means that our final tree should have `C7'` at the bottom, `C6'` above that, and so on, all in order.

![rebasing-multiple-branches_question](assets/rebasing-multiple-branches_question.jpg)

```
git rebase master bugFix
```

![rebasing-multiple-branches_answer1](assets/rebasing-multiple-branches_answer1.jpg)

```
git rebase bugFix side
```

![rebasing-multiple-branches_answer1](assets/rebasing-multiple-branches_answer2.jpg)

```
git rebase side another
```

![rebasing-multiple-branches_answer1](assets/rebasing-multiple-branches_answer3.jpg)

```
git rebase another master
```

![rebasing-multiple-branches_answer4](assets/rebasing-multiple-branches_answer4.jpg)

### 5.2 Multiple parents (lesson)

#### Specifying Parents

Like the `~` modifier, the `^` modifier also accepts an optional number after it.

Rather than specifying the number of generations to go back (what `~` takes), the modifier on `^` specifies which parent reference to follow from a merge commit. Remember that merge commits have multiple parents, so the path to choose is ambiguous.

Git will normally follow the "first" parent upwards from a merge commit, but specifying a number with `^` changes this default behavior.

#### Multiple parents (Demo #1)
Enough talking, let's see it in action.

Here we have a merge commit. If we checkout `master^` without the modifier, we will follow the first parent after the merge commit.

_(In our visuals, the first parent is positioned directly above the merge commit.)_

![multiple-parents_1](assets/multiple-parents_1.jpg)

Type: `git checkout master^`

![multiple-parents_2](assets/multiple-parents_2.jpg)

Easy -- this is what we are all used to.

#### Multiple parents (Demo #2)
Now let's try specifying the second parent instead...

![multiple-parents_3](assets/multiple-parents_3.jpg)

Type: `git checkout master^2`

![multiple-parents_4](assets/multiple-parents_4.jpg)

See? We followed the other parent upwards.

#### Multiple parents (Demo #3)
The `^` and `~` modifiers can make moving around a commit tree very powerful:

![multiple-parents_5](assets/multiple-parents_5.jpg)

Type: `git checkout HEAD~; git checkout HEAD^2; git checkout HEAD~2`

![multiple-parents_6](assets/multiple-parents_6.jpg)

Lightning fast!

#### Multiple parents (Demo #4)
Even crazier, these modifiers can be chained together! Check this out:

![multiple-parents_7](assets/multiple-parents_7.jpg)

Type: `git checkout HEAD~^2~2`

![multiple-parents_8](assets/multiple-parents_8.jpg)

The same movement as before, but all in one command.

#### Multiple parents (practice)
To complete this level, create a new branch called `bugWork` at `C2`.

Obviously it would be easy to specify the commit directly (with something like C6), but I challenge you to use the modifiers we talked about instead!

![multiple-parents_question](assets/multiple-parents_question.jpg)

```
git branch bugWork master^^2^
```

![multiple-parents_answer](assets/multiple-parents_answer.jpg)

### 5.3 Branch Spaghetti (lesson)

#### Branch Spaghetti (practice)
Here we have `master` that is a few commits ahead of branches `one` `two` and `three`. For whatever reason, we need to update these three other branches with modified versions of the last few commits on master.

Branch `one` needs a re-ordering and a deletion of `C5`. `two` needs pure reordering, and `three` only needs one commit!

![branch-spaghetti_question](assets/branch-spaghetti_question.jpg)

```
git checkout one
git cherry-pick C4 C3 C2
```

![branch-spaghetti_answer1](assets/branch-spaghetti_answer1.jpg)

```
git checkout two
git cherry-pick C5 C4 C3 C2
```

![branch-spaghetti_answer2](assets/branch-spaghetti_answer2.jpg)

```
git branch -f three C2
```

![branch-spaghetti_answer3](assets/branch-spaghetti_answer3.jpg)

## Chapter 6, Push & Pull -- Git Remotes

### 6.1 Clone Intro (lesson)

#### Git Remotes
Remote repositories aren't actually that complicated. In today's world of cloud computing it's easy to think that there's a lot of magic behind git remotes, but they are actually just copies of your repository on another computer. You can typically talk to this other computer through the Internet, which allows you to transfer commits back and forth.

That being said, remote repositories have a bunch of great properties:

* First and foremost, remotes serve as a great backup! Local git repositories have the ability to restore files to a previous state (as you know), but all that information is stored locally. By having copies of your git repository on other computers, you can lose all your local data and still pick up where you left off.

* More importantly, remotes make coding social! Now that a copy of your project is hosted elsewhere, your friends can contribute to your project (or pull in your latest changes) very easily.

It's become very popular to use websites that visualize activity around remote repos (like Github or Phabricator), but remote repositories _always_ serve as the underlying backbone for these tools. So it's important to understand them!

#### Our Command to create remotes
Up until this point, Learn Git Branching has focused on teaching the basics of local repository work (branching, merging, rebasing, etc). However now that we want to learn about remote repository work, we need a command to set up the environment for those lessons. `git clone` will be that command

***Technically, `git clone` in the real world is the command you'll use to create _local_ copies of remote repositories (from github for example). We use this command a bit differently in Learn Git Branching though -- `git clone` actually makes a remote repository out of your local one. Sure it's technically the opposite meaning of the real command, but it helps build the connection between cloning and remote repository work, so let's just run with it for now.***

#### Clone Intro (Demo)
Lets start slow and just look at what a remote repository looks like (in our visualization).

![clone-intro_1](assets/clone-intro_1.jpg)

Type: `git clone`

![clone-intro_2](assets/clone-intro_2.jpg)

There it is! Now we have a remote repository of our project. It looks pretty similar except for some visual changes to make the distinction apparent -- in later levels you'll get to see how we share work across these repositories.

#### Clone Intro (practice)
To finish this level, simply `git clone` your existing repository. The real learning will come in following lessons.

![clone-intro_1](assets/clone-intro_1.jpg)

```
git clone
```

![clone-intro_2](assets/clone-intro_2.jpg)

### 6.2 Remote Branches (lesson)

#### Git Remote Branches
Now that you've seen `git clone` in action, let's dive into what actually changed.

The first thing you may have noticed is that a new branch appeared in our local repository called `o/master`. This type of branch is called a _remote_ branch; remote branches have special properties because they serve a unique purpose.

Remote branches reflect the _state_ of remote repositories (since you last talked to those remote repositories). They help you understand the difference between your local work and what work is public -- a critical step to take before sharing your work with others.

Remote branches have the special property that when you check them out, you are put into detached `HEAD` mode. Git does this on purpose because you can't work on these branches directly; you have to work elsewhere and then share your work with the remote (after which your remote branches will be updated).

#### What is `o/`?
You may be wondering what the leading `o/` is for on these remote branches. Well, remote branches also have a (required) naming convention -- they are displayed in the format of:

* `<remote name>/<branch name>`

Hence, if you look at a branch named `o/master`, the branch name is `master` and the name of the remote is `o`.

Most developers actually name their main remote `origin`, not `o`. This is so common that git actually sets up your remote to be named `origin` when you `git clone` a repository.

***Unfortunately the full name of `origin` does not fit in our UI, so we use `o` as shorthand :( Just remember when you're using real git, your remote is probably going to be named `origin`!***

#### Remote Branches (Demo)
That's a lot to take in, so let's see all this in action.

Lets check out a remote branch and see what happens

![remote-branches_1](assets/remote-branches_1.jpg)

Type: `git checkout o/master; git commit`

![remote-branches_2](assets/remote-branches_2.jpg)

As you can see, git put us into detached `HEAD` mode and then did not update `o/master` when we added a new commit. This is because `o/master` will only update when the remote updates.

#### Remote Branches (practice)
To finish this level, commit once off of `master` and once after checking out `o/master`. This will help drive home how remote branches behave differently, and they only update to reflect the state of the remote.

![remote-branches_question](assets/remote-branches_question.jpg)

```
git commit
git checkout o/master
git commit
```

![remote-branches_answer](assets/remote-branches_answer.jpg)

### 6.3 Git Fetchin' (lesson)

#### Git Fetch
Working with git remotes really just boils down to transferring data _to_ and _from_ other repositories. As long as we can send commits back and forth, we can share any type of update that is tracked by git (and thus share work, new files, new ideas, love letters, etc.).

In this lesson we will learn how to fetch data _from_ a remote repository -- the command for this is conveniently named `git fetch`.

You'll notice that as we update our representation of the remote repository, our _remote_ branches will update to reflect that new representation. This ties into the previous lesson on remote branches

#### Git Fetchin' (Demo)
Before getting into the details of `git fetch`, let's see it in action! Here we have a remote repository that contains two commits that our local repository does not have.

![git-fetchin_1](assets/git-fetchin_1.jpg)

Type: `git fetch`

![git-fetchin_2](assets/git-fetchin_2.jpg)

There we go! Commits `C2` and `C3` were downloaded to our local repository, and our remote branch `o/master` was updated to reflect this.

#### What fetch does
`git fetch` performs two main steps, and two main steps only. It:

* downloads the commits that the remote has but are missing from our local repository, and...
* updates where our remote branches point (for instance, `o/master`)

`git fetch` essentially brings our _local_ representation of the remote repository into synchronization with what the _actual_ remote repository looks like (right now).

If you remember from the previous lesson, we said that remote branches reflect the state of the remote repositories _since_ you last talked to those remotes. `git fetch` is the way you talk to these remotes! Hopefully the connection between remote branches and `git fetch` is apparent now.

`git fetch` usually talks to the remote repository through the Internet (via a protocol like `http://` or `git://`).

#### What fetch doesn't do
`git fetch`, however, does not change anything about _your_ local state. It will not update your `master` branch or change anything about how your file system looks right now.

This is important to understand because a lot of developers think that running `git fetch` will make their local work reflect the state of the remote. It may download all the necessary data to do that, but it does _not_ actually change any of your local files. We will learn commands in later lessons to do just that :D

So at the end of the day, you can think of running `git fetch` as a download step.

#### Git Fetchin' (practice)
To finish the level, simply `git fetch` and download all the commits!

![git-fetchin_question](assets/git-fetchin_question.jpg)

```
git fetch
```

![git-fetchin_answer](assets/git-fetchin_answer.jpg)

### 6.4 Git Pullin' (lesson)

#### Git Pull
Now that we've seen how to fetch data from a remote repository with `git fetch`, let's update our work to reflect those changes!

There are actually many ways to do this -- once you have new commits available locally, you can incorporate them as if they were just normal commits on other branches. This means you could execute commands like:

* `git cherry-pick o/master`
* `git rebase o/master`
* `git merge o/master`
* etc., etc.

In fact, the workflow of _fetching_ remote changes and then _merging_ them is so common that git actually provides a command that does both at once! That command is `git pull`.

#### Git Pullin' (Demo #1)
Let's first see a fetch and a merge executed sequentially

![git-pullin_1](assets/git-pullin_1.jpg)

Type: `git fetch; git merge o/master`

![git-pullin_2](assets/git-pullin_2.jpg)

Boom -- we downloaded `C3` with a `fetch` and then merged in that work with `git merge o/master`. Now our `master` branch reflects the new work from the remote (in this case, named `origin`)

#### Git Pullin' (Demo #2)
What would happen if we used git pull instead?

![git-pullin_1](assets/git-pullin_1.jpg)

Type: `git pull`

![git-pullin_2](assets/git-pullin_2.jpg)

The same thing! That should make it very clear that `git pull` is essentially shorthand for a `git fetch` followed by a `merge` of whatever branch was just fetched.

#### Git Pullin' (practice)
We will explore the details of `git pull` later (including options and arguments), but for now let's try it out in the level.

![git-pullin_question](assets/git-pullin_question.jpg)

```
git pull
```

![git-pullin_answer](assets/git-pullin_answer.jpg)

### 6.5 Faking Teamwork (lesson)

#### Simulating collaboration

So here is the tricky thing -- for some of these upcoming lessons, we need to teach you how to pull down changes that were introduced in the remote.

***That means we need to essentially "pretend" that the remote was updated by one of your coworkers / friends / collaborators, sometimes on a specific branch or a certain number of commits.***

***In order to do this, we introduced the aptly-named command `git fakeTeamwork`! It's pretty self explanatory, let's see a demo...***

#### Faking Teamwork (Demo #1)
The default behavior of `fakeTeamwork` is to simply plop down a commit on master

![faking-teamwork_1](assets/faking-teamwork_1.jpg)

Type: `git fakeTeamwork`

![faking-teamwork_2](assets/faking-teamwork_2.jpg)

There we go -- the remote was updated with a new commit, and we haven't downloaded that commit yet because we haven't run `git fetch`.

#### Faking Teamwork (Demo #2)
You can also specify the number of commits or the branch by appending them to the command

![faking-teamwork_3](assets/faking-teamwork_3.jpg)

Type: `git fakeTeamwork foo 3`

![faking-teamwork_4](assets/faking-teamwork_4.jpg)

With one command we simulated a teammate pushing three commits to the `foo` branch on our remote

#### Faking Teamwork (practice)

The upcoming levels are going to be pretty difficult, so we're asking more of you for this level.

Go ahead and make a remote (with git clone), fake some changes on that remote, commit yourself, and then pull down those changes. It's like a few lessons in one!

![faking-teamwork_question](assets/faking-teamwork_question.jpg)

```
git clone
git fakeTeamwork master 2
git commit
git pull
```

![faking-teamwork_answer](assets/faking-teamwork_answer.jpg)

### 6.6 Git Pushin' (lesson)

#### Git Push
Ok, so I've fetched changes from remote and incorporated them into my work locally. That's great and all... but how do I share my awesome work with everyone else?

Well, the way to upload shared work is the opposite of downloading shared work. And what's the opposite of `git pull`? `git push`!

`git push` is responsible for uploading _your_ changes to a specified remote and updating that remote to incorporate your new commits. Once `git push` completes, all your friends can then download your work from the remote.

You can think of `git push` as a command to "publish" your work. It has a bunch of subtleties that we will get into shortly, but let's start with baby steps...

_note -- the behavior of `git push` with no arguments varies depending on one of git's settings called `push.default`. The default value for this setting depends on the version of git you're using, but we are going to use the `upstream` value in our lessons. This isn't a huge deal, but it's worth checking your settings before pushing in your own projects._

#### Git Pushin' (Demo)
Here we have some changes that the remote does not have. Let's upload them!

![git-pushin_1](assets/git-pushin_1.jpg)

Type: `git push`

![git-pushin_2](assets/git-pushin_2.jpg)

There we go -- the remote received commit `C2`, the branch `master` on the remote was updated to point at `C2`, and our _own_ reflection of the remote (`o/master`) was updated as well. Everything is in sync!

#### Git Pushin' (practice)
To finish this level, simply share two new commits with the remote. Strap in though, because these lessons are about to get a lot harder!

![git-pushin_question](assets/git-pushin_question.jpg)

```
git commit
git commit
git push
```

![git-pushin_answer](assets/git-pushin_answer.jpg)

### 6.7 Diverged History (lesson)

#### Diverged Work
So far we've seen how to `pull` down commits from others and how to `push` up our own changes. It seems pretty simple, so how can people get so confused?

The difficulty comes in when the history of the repository _diverges_. Before discussing the details of this, let's see an example...

Imagine you clone a repository on Monday and start dabbling on a side feature. By Friday you are ready to publish your feature -- but oh no! Your coworkers have written a bunch of code during the week that's made your feature out of date (and obsolete). They've also published these commits to the shared remote repository, so now _your_ work is based on an _old_ version of the project that's no longer relevant.

In this case, the command `git push` is ambiguous. If you run `git push`, should git change the remote repository back to what it was on Monday? Should it try to add your code in while not removing the new code? Or should it totally ignore your changes since they are totally out of date?

Because there is so much ambiguity in this situation (where history has diverged), git doesn't allow you to `push` your changes. It actually forces you to incorporate the latest state of the remote before being able to share your work.

#### Diverged History (Demo #1)
So much talking! Let's see this situation in action

![diverged-history_1](assets/diverged-history_1.jpg)

Type: `git push`

![diverged-history_1](assets/diverged-history_1.jpg)

See? Nothing happened because the command fails. `git push` fails because your most recent commit `C3` is based off of the remote at `C1`. The remote has since been updated to `C2` though, so git rejects your push

How do you resolve this situation? It's easy, all you need to do is base your work off of the most recent version of the remote branch.

There are a few ways to do this, but the most straightforward is to move your work via rebasing. Let's go ahead and see what that looks like.

#### Diverged History (Demo #2)
Now if we rebase before pushing instead...

![diverged-history_1](assets/diverged-history_1.jpg)

Type: `git fetch; git rebase o/master; git push`

![diverged-history_2](assets/diverged-history_2.jpg)

Boom! We updated our local representation of the remote with `git fetch`, rebased our work to reflect the new changes in the remote, and then pushed them with `git push`

Are there other ways to update my work when the remote repository has been updated? Of course! Let's check out the same thing but with `merge` instead.

Although `git merge` doesn't move your work (and instead just creates a merge commit), it's a way to tell git that you have incorporated all the changes from the remote. This is because the remote branch is now an _ancestor_ of your own branch, meaning your commit reflects all commits in the remote branch.

#### Diverged History (Demo #3)
Lets see this demonstrated...

Now if we merge instead of rebasing...

![diverged-history_1](assets/diverged-history_1.jpg)

Type: `git fetch; git merger o/master; git push`

![diverged-history_3](assets/diverged-history_3.jpg)

Boom! We updated our local representation of the remote with `git fetch`, _merged_ the new work into our work (to reflect the new changes in the remote), and then pushed them with `git push`

Awesome! Is there any way I can do this without typing so many commands?

Of course -- you already know `git pull` is just shorthand for a fetch and a merge. Conveniently enough, `git pull --rebase` is shorthand for a fetch and a rebase!

#### Diverged History (Demo #4)
Let's see these shorthand commands at work.

First with `--rebase`...

![diverged-history_1](assets/diverged-history_1.jpg)

Type: `git pull --rebase; git push`

![diverged-history_2](assets/diverged-history_2.jpg)

Same as before! Just a lot shorter.

And now with regular `pull`

![diverged-history_1](assets/diverged-history_1.jpg)

Type: `git pull; git push`

![diverged-history_3](assets/diverged-history_3.jpg)

Again, exact same as before!

#### Diverged History (practice)
This workflow of fetching, rebase/merging, and pushing is quite common. In future lessons we will examine more complicated versions of these workflows, but for now let's try this out.

In order to solve this level, take the following steps:

* Clone your repo
* Fake some teamwork with `git fakeTeamwork` (1 commit)
* Commit some work yourself (1 commit)
* Publish your work via ***rebasing***

![diverged-history_question](assets/diverged-history_question.jpg)

```
git clone
git fakeTeamwork
git commit
git pull --rebase
git push
```

![diverged-history_answer](assets/diverged-history_answer.jpg)

## Chapter 7, To Origin And Beyond -- Advanced Git Remotes

### 7.1 Push Master! (lesson)

#### Merging feature branches
Now that you're comfortable with fetching, pulling, and pushing, lets put these skills to the test with a new workflow.

It's common for developers on big projects to do all their work on feature branches (off of `master`) and then integrate that work only once it's ready. This is similar to the previous lesson (where side branches get pushed to the remote), but here we introduce one more step.

Some developers only push and pull when on the `master` branch -- that way `master` always stays updated to what is on the remote (`o/master`).

So for this workflow we combine two things:

* integrating feature branch work onto `master`, and
* pushing and pulling from the remote

#### Push Master! (Demo)
Let's see a refresher real quick of how to update `master` and push work.

![push-master_1](assets/push-master_1.jpg)

Type: `git pull --rebase; git push`

![push-master_2](assets/push-master_2.jpg)

We executed two commands here that:

* rebased our work onto new commits from remote, and
* published our work to the remote

#### Push Master! (practice)
This level is pretty hefty -- here is the general outline to solve:

There are three feature branches -- `side1` `side2` and `side3`
We want to push each one of these features, in order, to the remote
The remote has since been updated, so we will need to incorporate that work as well

![push-master_question](assets/push-master_question.jpg)

```
git fetch
```

![push-master_answer1](assets/push-master_answer1.jpg)

```
git rebase o/master side1
```

![push-master_answer2](assets/push-master_answer2.jpg)

```
git rebase side1 side2
```

![push-master_answer3](assets/push-master_answer3.jpg)

```
git rebase side2 side3
```

![push-master_answer4](assets/push-master_answer4.jpg)

```
git rebase side3 master
git push
```

![push-master_answer5](assets/push-master_answer5.jpg)

### 7.2 Merging with remotes (lesson)

#### Why not merge?
In order to push new updates to the remote, all you need to do is _incorporate_ the latest changes from the remote. That means you can either rebase _or_ merge in the remote branch (e.g. `o/master`).

So if you can do either method, why have the lessons focused on rebasing so far? Why is there no love for `merge` when working with remotes?

There's a lot of debate about the tradeoffs between merging and rebasing in the development community. Here are the general pros / cons of rebasing:

Pros:

* Rebasing makes your commit tree look very clean since everything is in a straight line

Cons:

* Rebasing modifies the (apparent) history of the commit tree.

For example, commit `C1` can be rebased past `C3`. It then appears that the work for `C1'` came after `C3` when in reality it was completed beforehand.

Some developers love to preserve history and thus prefer merging. Others (like myself) prefer having a clean commit tree and prefer rebasing. It all comes down to preferences :D

#### Merging with remotes (practice)

For this level, let's try to solve the previous level but with _merging_ instead. It may get a bit hairy but it illustrates the point well.

![merging-with-remotes_question](assets/merging-with-remotes_question.jpg)

```
git checkout master
git pull
```

![merging-with-remotes_answer1](assets/merging-with-remotes_answer1.jpg)

```
git merge side1
```

![merging-with-remotes_answer2](assets/merging-with-remotes_answer2.jpg)

```
git merge side2
```

![merging-with-remotes_answer3](assets/merging-with-remotes_answer3.jpg)

```
git merge side3
```

![merging-with-remotes_answer4](assets/merging-with-remotes_answer4.jpg)

```
git push
```

![merging-with-remotes_answer5](assets/merging-with-remotes_answer5.jpg)

### 7.3 Remote Tracking (lesson)

#### Remote-Tracking branches
One thing that might have seemed "magical" about the last few lessons is that git knew the `master` branch was related to `o/master`. Sure these branches have similar names and it might make logical sense to connect the `master` branch on the remote to the local `master` branch, but this connection is demonstrated clearly in two scenarios:

* During a `pull` operation, commits are downloaded onto `o/master` and then _merged_ into the `master` branch. The implied target of the merge is determined from this connection.
* During a `push` operation, work from the `master` branch was pushed onto the remote's `master` branch (which was then represented by `o/master` locally). The _destination_ of the push is determined from the connection between `master` and `o/master`.

#### Remote tracking
Long story short, this connection between `master` and `o/master` is explained simply by the "remote tracking" property of branches. The `master` branch is set to track `o/master` -- this means there is an implied merge target and implied push destination for the `master` branch.

You may be wondering how this property got set on the `master` branch when you didn't run any commands to specify it. Well, when you clone a repository with git, this property is actually set for you automatically.

During a clone, git creates a remote branch for every branch on the remote (aka branches like `o/master`). It then creates a local branch that tracks the currently active branch on the remote, which is `master` in most cases.

Once `git clone` is complete, you only have one local branch (so you aren't overwhelmed) but you can see all the different branches on the remote (if you happen to be very curious). It's the best of both worlds!

This also explains why you may see the following command output when cloning:

`local branch "master" set to track remote branch "o/master"`

#### Can I specify this myself?
Yes you can! You can make any arbitrary branch track `o/master`, and if you do so, that branch will have the same implied push destination and merge target as `master`. This means you can run `git push` on a branch named `totallyNotMaster` and have your work pushed to the `master` branch on the remote!

There are two ways to set this property. The first is to checkout a new branch by using a remote branch as the specified ref. Running

`git checkout -b totallyNotMaster o/master`

Creates a new branch named `totallyNotMaster` and sets it to track `o/master`.

#### Remote Tracking (Demo #1)
Enough talking, let's see a demonstration! We will checkout a new branch named `foo` and set it to track `master` on the remote.

![remote-tracking_1](assets/remote-tracking_1.jpg)

Type: `git checkout -b foo o/master; git pull`

![remote-tracking_2](assets/remote-tracking_2.jpg)

As you can see, we used the implied merge target of `o/master` to update the `foo` branch. Note how master doesn't get updated!!

#### Remote Tracking (Demo #2)

This also applies for `git push`

![remote-tracking_3](assets/remote-tracking_3.jpg)

Type: `git checkout -b foo o/master; git commit; git push`

![remote-tracking_4](assets/remote-tracking_4.jpg)

Boom. We pushed our work to the `master` on the remote even though our branch was named something totally different

#### Way #2
Another way to set remote tracking on a branch is to simply use the `git branch -u` option.
Running:

`git branch -u o/master foo`

will set the `foo` branch to track `o/master`. If `foo` is currently checked out you can even leave it off:

`git branch -u o/master`

#### Remote Tracking (Demo #3)
Let's see this other way of specifying remote tracking real quick...

![remote-tracking_5](assets/remote-tracking_5.jpg)

Type: `git branch -u o/master foo; git commit; git push`

![remote-tracking_6](assets/remote-tracking_6.jpg)

Same as before, just a more explicit command. Sweet!

#### Remote Tracking (practice)
Ok! For this level let's push work onto the `master` branch on remote while _not_ checked out on `master` locally.

![remote-tracking_question](assets/remote-tracking_question.jpg)

```
git checkout -b side o/master
git commit
git pull --rebase
git push
```

![remote-tracking_answer](assets/remote-tracking_answer.jpg)

### 7.4 Git push arguments (lesson)

#### Push arguments
Great! Now that you know about remote tracking branches we can start to uncover some of the mystery behind how git push, fetch, and pull work. We're going to tackle one command at a time but the concepts between them are very similar.

First we'll look at `git push`. You learned in the remote tracking lesson that git figured out the remote _and_ the branch to push to by looking at the properties of the currently checked out branch (the remote that it "tracks"). This is the behavior with no arguments specified, but git push can optionally take arguments in the form of:

`git push <remote> <place>`

What is a `<place>` parameter you say? We'll dive into the specifics soon, but first an example. Issuing the command:

`git push origin master`

translates to this in English:

_Go to the branch named "master" in my repository, grab all the commits, and then go to the branch "master" on the remote named "origin." Place whatever commits are missing on that branch and then tell me when you're done._

By specifying `master` as the "place" argument, we told git where the commits will _come from_ and where the commits _will go_. It's essentially the "place" or "location" to synchronize between the two repositories.

Keep in mind that since we told git everything it needs to know (by specifying both arguments), it totally ignores where we are checked out!

#### Git push arguments (demo #1)
Let's see an example of specifying the arguments. Note the location where we are checked out in this example.

![git-push-arguments_1](assets/git-push-arguments_1.jpg)

Type: `git checkout C0; git push origin master`

![git-push-arguments_2](assets/git-push-arguments_2.jpg)

There we go! `master` got updated on the remote since we specified those arguments.

#### Git push arguments (demo #2)
What if we hadn't specified the arguments? What would happen?

![git-push-arguments_3](assets/git-push-arguments_3.jpg)

Type: `git checkout C0; git push`

![git-push-arguments_4](assets/git-push-arguments_4.jpg)

The command fails (as you can see), since `HEAD` is not checked out on a remote-tracking branch.

#### Git push arguments (practice)
Ok, for this level let's update both `foo` and `master` on the remote. The twist is that `git checkout` is disabled for this level!

***Note: The remote branches are labeled with `o/` prefixes because the full `origin/` label does not fit in our UI. Don't worry about this... simply use `origin` as the name of the remote like normal.***

![git-push-arguments_question](assets/git-push-arguments_question.jpg)

```
git push origin master
git push origin foo
```

![git-push-arguments_answer](assets/git-push-arguments_answer.jpg)

### 7.5 Git push arguments -- Expanded! (lesson)

#### `<place>` argument details
Remember from the previous lesson that when we specified `master` as the place argument for `git push`, we specified both the _source_ of where the commits would come from and the _destination_ of where the commits would go.

You might then be wondering -- what if we wanted the source and destination to be different? What if you wanted to push commits from the `foo` branch locally onto the `bar` branch on remote?

Well unfortunately that's impossible in git... just kidding! Of course it's possible :)... git has tons and tons of flexibility (almost too much).

In order to specify both the source and the destination of `<place>`, simply join the two together with a colon:

`git push origin <source>:<destination>`

This is commonly referred to as a **colon refspec**. Refspec is just a fancy name for a location that git can figure out (like the branch `foo` or even just `HEAD~1`)

Once you are specifying both the source and destination independently, you can get quite fancy and precise with remote commands. Let's see a demo!

#### Git push arguments -- Expanded! (demo #1)
Remember, `source` is any location that git will understand:

![git-push-arguments--expanded_1](assets/git-push-arguments--expanded_1.jpg)

Type: `git push origin foo^:master`

![git-push-arguments--expanded_2](assets/git-push-arguments--expanded_2.jpg)

Woah! That's a pretty trippy command but it makes sense -- git resolved `foo^` into a location, uploaded whatever commits that weren't present yet on the remote, and then updated destination.

#### Git push arguments -- Expanded! (demo #2)
What if the destination you want to push doesn't exist? No problem! Just give a branch name and git will create the branch on the remote for you.

![git-push-arguments--expanded_3](assets/git-push-arguments--expanded_3.jpg)

Type: `git push origin master:newBranch`

![git-push-arguments--expanded_4](assets/git-push-arguments--expanded_4.jpg)

Sweet, that's pretty slick :D

#### Git push arguments -- Expanded! (practice)
For this level, try to get to the end goal state shown in the visualization, and remember the format of:

`<source>:<destination>`

![git-push-arguments--expanded_question](assets/git-push-arguments--expanded_question.jpg)

```
git push origin master^:foo
```

![git-push-arguments--expanded_answer1](assets/git-push-arguments--expanded_answer1.jpg)

```
git push origin foo:master
```

![git-push-arguments--expanded_answer2](assets/git-push-arguments--expanded_answer2.jpg)

### 7.6 Fetch arguments (lesson)

#### Git fetch arguments
So we've just learned all about `git push` arguments, this cool `<place>` parameter, and even colon refspecs (`<source>:<destination>`). Can we use all this knowledge for `git fetch` as well?

You betcha! The arguments for `git fetch` are actually _very, very_ similar to those for `git push`. It's the same type of concepts but just applied in the opposite direction (since now you are downloading commits rather than uploading).

Let's go over the concepts one at a time...

#### The `<place>` parameter
If you specify a place with git fetch like in the following command:

`git fetch origin foo`

Git will go to the `foo` branch on the remote, grab all the commits that aren't present locally, and then plop them down onto the `o/foo` branch locally.

#### Fetch arguments (demo #1)
Let's see this in action (just as a refresher). By specifying a place...

![fetch-arguments_1](assets/fetch-arguments_1.jpg)

Type: `git fetch origin foo`

![fetch-arguments_2](assets/fetch-arguments_2.jpg)

We download only the commits from `foo` and place them on `o/foo`

You might be wondering -- why did git plop those commits onto the `o/foo` remote branch rather than just plopping them onto my local `foo` branch? I thought the `<place>` parameter is a place that exists both locally and on the remote?

Well git makes a special exception in this case because you might have work on the `foo` branch that you don't want to mess up!! This ties into the earlier lesson on `git fetch` -- it doesn't update your local non-remote branches, it only downloads the commits (so you can inspect / merge them later).

"Well in that case, what happens if I explicitly define both the source and destination with `<source>:<destination>`?"

If you feel passionate enough to fetch commits _directly_ onto a local branch, then yes you can specify that with a colon refspec. You can't fetch commits onto a branch that is checked out, but otherwise git will allow this.

Here is the only catch though -- `<source>` is now a place on the _remote_ and `<destination>` is a _local_ place to put those commits. It's the exact opposite of `git push`, and that makes sense since we are transferring data in the opposite direction!

***That being said, developers rarely do this in practice. I'm introducing it mainly as a way to conceptualize how `fetch` and `push` are quite similar, just in opposite directions.***

#### Fetch arguments (demo #2)
Let's see this craziness in action:

![fetch-arguments_3](assets/fetch-arguments_3.jpg)

Type: `git fetch origin foo~1:bar`

![fetch-arguments_4](assets/fetch-arguments_4.jpg)

Wow! See, git resolved `foo~1` as a place on the origin and then downloaded those commits to `bar` (which was a local branch). Notice how `foo` and `o/foo` were not updated since we specified a destination.

#### Fetch arguments (demo #3)
What if the destination doesn't exist before I run the command? Let's see the last slide but without `bar` existing beforehand.

![fetch-arguments_5](assets/fetch-arguments_5.jpg)

Type: `git fetch origin foo~1:bar`

![fetch-arguments_6](assets/fetch-arguments_6.jpg)

See, it's JUST like `git push`. Git made the destination locally before fetching, just like git will make the destination on remote before pushing (if it doesn't exist).

#### Fetch arguments (demo #4)
No args?

If `git fetch` receives no arguments, it just downloads all the commits from the remote onto all the remote branches...

![fetch-arguments_7](assets/fetch-arguments_7.jpg)

Type: `git fetch`

![fetch-arguments_8](assets/fetch-arguments_8.jpg)

Pretty simple, but worth going over just once.

#### Fetch arguments (practice)
Ok, enough talking! To finish this level, fetch just the specified commits in the goal visualization. Get fancy with those commands!

You will have to specify the source and destination for both fetch commands.

![fetch-arguments_question](assets/fetch-arguments_question.jpg)

```
git fetch origin master~1:foo
```

![fetch-arguments_8](assets/fetch-arguments_answer1.jpg)

```
git fetch origin foo:master
```

![fetch-arguments_8](assets/fetch-arguments_answer2.jpg)

```
git checkout foo
git merge master
```

![fetch-arguments_8](assets/fetch-arguments_answer3.jpg)

### 7.7 Source of nothing (lesson)

#### Oddities of `<source>`
Git abuses the `<source>` parameter in two weird ways. These two abuses come from the fact that you can technically specify "nothing" as a valid `source` for both `git push` and `git fetch`. The way you specify nothing is via an empty argument:

* `git push origin :side`
* `git fetch origin :bugFix`

Let's see what these do...

#### Source of nothing (demo #1)
What does pushing "nothing" to a remote branch do? It deletes it!

![source-of-nothing_1](assets/source-of-nothing_1.jpg)

Type: `git push origin :foo`

![source-of-nothing_2](assets/source-of-nothing_2.jpg)

There, we successfully deleted the `foo` branch on remote by pushing the concept of "nothing" to it. That kinda makes sense...

#### Source of nothing (demo #2)
Finally, fetching "nothing" to a place locally actually makes a new branch

![source-of-nothing_2](assets/source-of-nothing_2.jpg)

Type `git fetch origin :bar`

![source-of-nothing_3](assets/source-of-nothing_3.jpg)

Very odd / bizarre, but whatever. That's git for you!

#### Source of nothing (practice)
This is a quick level -- just delete one remote branch and create a new branch with `git fetch` to finish!

![source-of-nothing_3](assets/source-of-nothing_question.jpg)

```
git push origin :foo
git fetch origin :bar
```

![source-of-nothing_answer](assets/source-of-nothing_answer.jpg)

### 7.8 Pull arguments (lesson)

#### Git pull arguments
Now that you know pretty much _everything_ there is to know about arguments for `git fetch` and `git push`, there's almost really nothing left to cover for `git pull` :)

That's because `git pull` at the end of the day is _really_ just shorthand for a fetch followed by merging in whatever was just fetched. You can think of it as running git fetch with the _same_ arguments specified and then merging in _where_ those commits ended up.

This applies even when you use crazy-complicated arguments as well. Let's see some examples:

Here are some equivalent commands in git:

`git pull origin foo` is equal to:

`git fetch origin foo; git merge o/foo`

And...

`git pull origin bar~1:bugFix` is equal to:

`git fetch origin bar~1:bugFix; git merge bugFix`

See? `git pull` is really just shorthand for fetch + merge, and all `git pull` cares about is where the commits ended up (the `destination` argument that it figures out during fetch).

#### Pull arguments (demo #1)
If we specify the place to fetch, everything happens as before with fetch but we merge in whatever was just fetched

![pull-arguments_1](assets/pull-arguments_1.jpg)

Type: `git pull origin master`

![pull-arguments_2](assets/pull-arguments_2.jpg)

See! by specifying `master` we downloaded commits onto `o/master` just as normal. Then we merged `o/master` to where we are, _regardless_ of what was currently checked out.

#### Pull arguments (demo #2)
Does it work with source and destination too? You bet! Let's see that:

![pull-arguments_3](assets/pull-arguments_3.jpg)

Type: `git pull origin master:foo`

![pull-arguments_4](assets/pull-arguments_4.jpg)

Wow, that's a TON in one command. We created a new branch locally named `foo`, downloaded commits from remote's master onto that branch `foo`, and then merged that branch into our currently checked out branch `bar`.
>It's over 9000!!!

#### Pull arguments (practice)
Ok to finish up, attain the state of the goal visualization. You'll need to download some commits, make some new branches, and merge those branches into other branches, but it shouldn't take many commands :P

![pull-arguments_4](assets/pull-arguments_question.jpg)

```
git pull origin bar:foo
```

![pull-arguments_4](assets/pull-arguments_answer1.jpg)

```
git pull origin master:side
```

![pull-arguments_4](assets/pull-arguments_answer2.jpg)
